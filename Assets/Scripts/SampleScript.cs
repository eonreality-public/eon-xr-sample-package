using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using System;
using Utils.Responses;

public class SampleScript : MonoBehaviour
{
    
    [SerializeField] protected Button ReportSetScoreTab;
    [SerializeField] protected Button RightsTab;
    [SerializeField] protected GameObject ReportSetScorePanel;
    [SerializeField] protected GameObject RightsPanel;
    [SerializeField] protected InputField DurationInput;
    [SerializeField] protected InputField ScoreInput;
    [SerializeField] protected InputField FailedLessonStepsInput;
    [SerializeField] protected InputField CriticalErrorsInput;
    [SerializeField] protected InputField LessonDeviationsInput;
    [SerializeField] protected Text ReportSetScoreResponse;
    [SerializeField] protected Button ReportSetScoreCall;
    [SerializeField] protected Text RightsResponse;
    [SerializeField] protected Button RightsCall;

    private API ApiObject = new API();

    void Start()
    {
		ReportSetScoreTab.onClick.AddListener(ReportSetScoreTabClick);
		RightsTab.onClick.AddListener(RightsTabClick);

		ReportSetScoreCall.onClick.AddListener(ReportSetScoreCallClick);
		RightsCall.onClick.AddListener(RightsCallClick);

        ApiObject.BaseUrl = CmdArgs.GetBaseUrl();
        ApiObject.OnSuccessAction = OnSuccess;
        ApiObject.OnFailAction = OnFail;
    }

    void ReportSetScoreTabClick()
    {
        ReportSetScorePanel.SetActive(true);
        RightsPanel.SetActive(false);
    }
    
    void RightsTabClick()
    {
        ReportSetScorePanel.SetActive(false);
        RightsPanel.SetActive(true);
    }

    void ReportSetScoreCallClick()
    {
        try {
            int duration = int.Parse(DurationInput.text); 
            int score = int.Parse(ScoreInput.text); 
            string failedLessonSteps = FailedLessonStepsInput.text; 
            string criticalErrors = CriticalErrorsInput.text; 
            string lessonDeviations = LessonDeviationsInput.text; 
            ApiObject.PackageReportSetScore(CmdArgs.GetToken(), duration, score, failedLessonSteps, criticalErrors, lessonDeviations);
        } catch (Exception e) {
            Debug.Log(e);
        }
    }

    void RightsCallClick()
    {
        try {
            ApiObject.PackageRights(CmdArgs.GetToken());
        } catch (Exception e) {
            Debug.Log(e);
        }
    }

    void OnSuccess(Endpoint t, object o)
    {
        var data = o as PackageResponse;
        if (t == Endpoint.PackageReportSetScore)
        {
            ReportSetScoreResponse.text = $"Status = {data.Status}, Message = {data.Message}";
        }
        else
        {
            if (data.Status.Equals("1"))
            {
                RightsResponse.text = $"Status = {data.Status}, Message = {data.Message}";
            } else 
            {
                RightsResponse.text = "You do not have  right to access this application.";
            }
        }
    }

    void OnFail(Endpoint t, string error)
    {
        if (t == Endpoint.PackageReportSetScore)
            ReportSetScoreResponse.text = error;
        else
            RightsResponse.text = error;
    }
}
