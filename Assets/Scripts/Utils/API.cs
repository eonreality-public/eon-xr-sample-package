using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Text;
using Newtonsoft.Json;
using Utils.Responses;

namespace Utils
{
    public enum Endpoint {
        PackageReportSetScore,
        PackageRights
    }
    public class API
    {
        /// Api base url
        public string BaseUrl = "https://api.eon-xr.com";

        /// Delegates for api success and fail listeners
        public delegate void OnSuccess(Endpoint t, object o);
        public delegate void OnFail(Endpoint t, string error);
        
        public OnSuccess OnSuccessAction;
        public OnFail OnFailAction;

        private void Call(Endpoint endpoint, Dictionary<string, object> data)
        {
            var dataStr = JsonConvert.SerializeObject(data);
            byte[] encodedPayload = new UTF8Encoding().GetBytes(dataStr);

            UnityWebRequest webRequest = new UnityWebRequest($"{BaseUrl}/PackageExternal/{endpoint.ToString()}", "POST");
            webRequest.uploadHandler = (UploadHandler) new UploadHandlerRaw(encodedPayload);
            webRequest.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            webRequest.SetRequestHeader("Content-Type", "application/json");
            webRequest.SetRequestHeader("cache-control", "no-cache");
                
            UnityWebRequestAsyncOperation requestHandel = webRequest.SendWebRequest();
            requestHandel.completed += delegate(AsyncOperation pOperation) {
                try {
                    var response = JsonConvert.DeserializeObject<PackageResponse>(webRequest.downloadHandler.text);
                    OnSuccessAction?.Invoke(endpoint, response);
                } catch (Exception e) {
                    OnFailAction?.Invoke(endpoint, e.Message);
                }
            };
        }

        public void PackageReportSetScore(string token, int duration, int score, string failedLessonSteps, string criticalErrors, string lessonDeviations)
        {
            if (token == string.Empty) 
            {
                OnFailAction?.Invoke(Endpoint.PackageReportSetScore, "Token is missing.");
            } else if (BaseUrl == string.Empty) 
            {
                OnFailAction?.Invoke(Endpoint.PackageReportSetScore, "Base url is missing.");
            } else 
            {
                var data = new Dictionary<string, object>{
                    {"token", token},
                    {"duration", duration},
                    {"score", score},
                    {"failedLessonSteps", failedLessonSteps},
                    {"criticalErrors", criticalErrors},
                    {"lessonDeviations", lessonDeviations}
                };
                Call(Endpoint.PackageReportSetScore, data);
            }
        }

        public void PackageRights(string token)
        {
            if (token == string.Empty) 
            {
                OnFailAction?.Invoke(Endpoint.PackageRights, "Token is missing.");
            } else if (BaseUrl == string.Empty) 
            {
                OnFailAction?.Invoke(Endpoint.PackageRights, "Base url is missing.");
            } else 
            {
                var data = new Dictionary<string, object>{
                    {"token", token}
                };
                Call(Endpoint.PackageRights, data);
            }
        }
    } 
}
