using System;
using UnityEngine;

namespace Utils
{
    public class CmdArgs
    {
        /// .exe --baseurl "https://eon-xr.com" --token "a570a9e74bbc43c3b180e63b0afb704c"
        public static string GetBaseUrl() {
            string[] lines = Environment.CommandLine.Split(' ');
            if (lines.Length > 3)
            {
                return lines[lines.Length - 3];
            }
            return string.Empty;
        }

        public static string GetToken() {
            string[] lines = Environment.CommandLine.Split(' ');
            if (lines.Length > 3)
            {
                return lines[lines.Length - 1];
            }
            return string.Empty;
        }
    } 
}