using Newtonsoft.Json;
using System;

namespace Utils.Responses
{
    public class PackageResponse
    {
        [JsonIgnore] private string status;
        [JsonIgnore] private string message;

        [JsonProperty("status")]
        public string Status
        {
            get => status;
            set
            {
                if(string.Compare(value, status, StringComparison.Ordinal) != 0)
                {
                    status = value;
                }
            }
        }

        [JsonProperty("message")]
        public string Message
        {
            get => message;
            set
            {
                if(string.Compare(value, message, StringComparison.Ordinal) != 0)
                {
                    message = value;
                }
            }
        }
    }
}
